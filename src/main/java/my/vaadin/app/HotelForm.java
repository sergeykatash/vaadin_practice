package my.vaadin.app;

import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import my.vaadin.app.backend.Category;
import my.vaadin.app.backend.Hotel;
import my.vaadin.app.backend.HotelService;
/**
*/
public class HotelForm extends FormLayout {
	
	private TextField name = new TextField("Name");
	private TextField address = new TextField("Address");
	private TextField rating = new TextField("Rating");
	private NativeSelect<Category> category = new NativeSelect<>("Category");
	private PaymentField payment = new PaymentField();
	private DateField operatesFrom = new DateField("OperatesFrom");
	private TextField url = new TextField("Url");
	private TextArea description = new TextArea("Description");
	private Button save = new Button("Save");
	private Button close = new Button("Cancel");
	private Binder<Hotel> binder = new Binder<>(Hotel.class);
	
	private HotelService service = HotelService.getInstance();
	private Hotel hotel;
	private HotelView view;
	
	public HotelForm(HotelView view) {
		this.view = view;
		
		name.setDescription("name of a hotel");
		name.setId("name");
		address.setDescription("address of a hotel");
		address.setId("address");
		rating.setDescription("rating of a hotel");
		rating.setId("rating");
		category.setDescription("catogory of a hotel");
		category.setId("category");
		operatesFrom.setDescription("date of start");
		operatesFrom.setId("operatesFrom");
		url.setDescription("site of a hotel");
		url.setId("url");
		description.setDescription("description of a hotel");
		payment.setDescription("Payment method");
		payment.setCaption("Payment method");
		
		setSizeUndefined();
		HorizontalLayout buttons = new HorizontalLayout(save, close);
		addComponents(name, address, rating, category, operatesFrom, url, description , payment, buttons);
		
		category.setItems(service.getCategoriesL());
		save.setId("saveHotel");
		save.setStyleName(ValoTheme.BUTTON_PRIMARY);
		save.setClickShortcut(KeyCode.ENTER);

		bindFields();
		
		payment.addValueChangeListener(v -> {
        	Payment oldPercent = v.getOldValue(); Payment newPercent = v.getValue();
        	String oldPercentDesc; String newPercentDesc;
        	if (oldPercent.getPayment() == null){
			    oldPercentDesc = "Cash"	;
			    }else {
			    oldPercentDesc =  oldPercent.getPayment()+ "%";
			    }
        	if (newPercent.getPayment() == null){
        		newPercentDesc = "Cash"	;
    			}else {
    			newPercentDesc = newPercent.getPayment()+ "%";
    			}
           Notification.show("Percent was: " + oldPercentDesc + "; is: " + newPercentDesc);
        });
				
		save.addClickListener(e -> this.save());
		close.addClickListener(e -> this.closeHotelForm());
		
	}
	//bind fields with class hotel
	private void bindFields(){
		binder.forField(rating).withConverter(new StringToIntegerConverter("Must be digits!")).asRequired("not be empty")
		.withValidator(v -> v>0, "Rating must contain at least one star" )
		.withValidator(v -> v<6, "5 is the max rating" )
		.bind(Hotel::getRating, Hotel::setRating);
		binder.forField(name).asRequired("not be empty").bind(Hotel::getName, Hotel::setName);
		binder.forField(address).asRequired("not be empty").bind(Hotel::getAddress, Hotel::setAddress);
		binder.forField(category).asRequired("not be empty").bind(Hotel::getCategory, Hotel::setCategory);
		binder.forField(url).asRequired("not be empty").bind(Hotel::getUrl, Hotel::setUrl);
		binder.forField(description).bind(Hotel::getDescription, Hotel::setDescription);
		binder.forField(operatesFrom).asRequired("not be empty").withConverter(new DateToLongConverter())
		.withValidator(v -> v>0, "not today")
		.bind(Hotel::getOperatesFrom, Hotel::setOperatesFrom);
		binder.forField(payment).asRequired("not be empty")
		.withValidator(v -> (v.getPayment()>0 && v.getPayment()<=100), "from 1 to 100")
		.bind(Hotel::getPayment, Hotel::setPayment);

	}
	
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
		binder.setBean(hotel);
		category.setItems(service.getCategoriesL());
	    setVisible(true);
	    name.selectAll();
	}
	
	public void closeHotelForm() {
		binder.removeBean();
	    setVisible(false);
	}

	private void save() {
		service.save(hotel);
		view.updateListHotels();
	    setVisible(false);
	}
	
	
	
	

}
