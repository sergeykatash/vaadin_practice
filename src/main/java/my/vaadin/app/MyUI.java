package my.vaadin.app;

import javax.servlet.annotation.WebServlet;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality
 */
@Theme("mytheme")
@SpringUI
public class MyUI extends UI {
	
	@Autowired
    private SpringViewProvider viewProvider;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
    	final VerticalLayout layout = new VerticalLayout();
    	
    	
    	final VerticalLayout container = new VerticalLayout();
    	
    	Navigator navigator = new Navigator(this, container);
    	navigator.addProvider(viewProvider);
        
    	MenuBar menu = new MenuBar();
        menu.addItem("Hotels",  v -> {
        	navigator.navigateTo(HotelView.VIEW_NAME);
        });
        menu.addItem("", null);
        menu.addItem("Categories", null, v -> {
        	navigator.navigateTo(CategoryView.VIEW_NAME);
        });
       
        layout.addComponents(menu, container);
        setContent(layout);

    }

}
