package my.vaadin.app;

import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.PopupView;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.components.grid.MultiSelectionModel;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.themes.ValoTheme;

import my.vaadin.app.backend.Category;
import my.vaadin.app.backend.Hotel;
import my.vaadin.app.backend.HotelService;

@SpringView(name = HotelView.VIEW_NAME)
public class HotelView extends VerticalLayout implements View {
	public static final String VIEW_NAME = "";
	private HotelService service = HotelService.getInstance();
	private Grid<Hotel> grid = new Grid<>(Hotel.class);
	private TextField filterName = new TextField();
	private TextField filterAddress = new TextField();
	private HotelForm form = new HotelForm(this);
	private PopupContentView popupContent = new PopupContentView(this);
	
	@PostConstruct
    void init() {
		
        PopupView popup = new PopupView(null, popupContent);
        popup.setHideOnMouseOut(false);
		
		MultiSelectionModel<Hotel> selectionModel = (MultiSelectionModel<Hotel>) grid
    			.setSelectionMode(SelectionMode.MULTI);
    	
    	grid.setColumns("name","category", "rating", "operatesFrom", "address", "description");
        grid.addColumn(hotel ->"<a href='" + hotel.getUrl() + "' target='_blank'>more info</a>", new HtmlRenderer()).setCaption("Link");
        
        Button addHotelBtn = new Button("New");
        addHotelBtn.setId("addHotelBtn");
        Button editHotelBtn = new Button("Edit");
        editHotelBtn.setEnabled(false);
        Button deleteHotelBtn = new Button("Delete");
        deleteHotelBtn.setEnabled(false);
        Button bulkHotelBtn = new Button("Bulk Update");
        bulkHotelBtn.setEnabled(false);
        
        //search hotels by name
        filterName.setPlaceholder("filter by name...");
        filterName.addValueChangeListener(e -> updateListHotels());
        filterName.setValueChangeMode(ValueChangeMode.LAZY);
        
        Button clearFilterNameBtn = new Button(VaadinIcons.CLOSE);
        clearFilterNameBtn.setDescription("Clear the current filter");
        clearFilterNameBtn.addClickListener(e -> filterName.clear());
        
        CssLayout filteringN = new CssLayout();
        filteringN.addComponents(filterName, clearFilterNameBtn);
        filteringN.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
        
        //search hotels by address
        filterAddress.setPlaceholder("filter by address...");
        filterAddress.addValueChangeListener(e -> updateListHotels());
        filterAddress.setValueChangeMode(ValueChangeMode.LAZY);
        
        Button clearFilterAddressBtn = new Button(VaadinIcons.CLOSE);
        clearFilterAddressBtn.setDescription("Clear the current filter");
        clearFilterAddressBtn.addClickListener(e -> filterAddress.clear());
        
        CssLayout filteringAdd = new CssLayout();
        filteringAdd.addComponents(filterAddress, clearFilterAddressBtn);
        filteringAdd.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
    	
        //add new hotel
        addHotelBtn.addClickListener(click -> {
            grid.asMultiSelect().clear();
            form.setHotel(new Hotel());
        });
        
        //edit current hotel
        editHotelBtn.addClickListener(click -> {
        	form.setHotel((Hotel)grid.asMultiSelect().getSelectedItems().iterator().next());
        });
        
        //delete selected hotel(s)
        deleteHotelBtn.addClickListener(click -> {
        	Iterator<Hotel> it = grid.asMultiSelect().getSelectedItems().iterator();
        	while(it.hasNext()){
        		service.delete(it.next());
        	}
        	updateListHotels();
        });
        
        //bulk update button
        bulkHotelBtn.addClickListener(click -> {
        	popupContent.setHotels(grid.asMultiSelect().getSelectedItems());
        	popup.setPopupVisible(true);
        });
                    
        HorizontalLayout toolbar = new HorizontalLayout(filteringN, filteringAdd);
        HorizontalLayout buttons = new HorizontalLayout(addHotelBtn, editHotelBtn, deleteHotelBtn, bulkHotelBtn);
        HorizontalLayout main = new HorizontalLayout(grid, form);
        main.setSizeFull();
        grid.setSizeFull();
        popup.setSizeFull();
        main.setExpandRatio(grid, 1);
        addComponents(toolbar, buttons, main, popup);
        updateListHotels();
        form.setVisible(false);
        
        //availability of buttons
        grid.asMultiSelect().addValueChangeListener(v -> {
        	form.closeHotelForm();
        	int count = selectionModel.getSelectedItems().size();
        	editHotelBtn.setEnabled(count == 1);
        	deleteHotelBtn.setEnabled(count != 0);
        	bulkHotelBtn.setEnabled(count > 1);
        });
    }
	
	public void updateListHotels() {
		List<Hotel> hotels = service.findAll(filterName.getValue(), filterAddress.getValue());
        grid.setItems(hotels);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		
	}
	

}
