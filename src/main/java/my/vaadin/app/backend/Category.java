package my.vaadin.app.backend;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
/**
*/

@Entity
@Table (name = "CATEGORY")
public class Category extends AbstractEntity implements Serializable, Cloneable {
	
	@NotNull(message = "fill the name of category")
	private String name;
	
	public Category() {
		this("");
	}
	
	public Category(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return name; 
	}
}
