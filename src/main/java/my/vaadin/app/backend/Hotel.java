package my.vaadin.app.backend;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import my.vaadin.app.Payment;


@SuppressWarnings("serial")
@Entity
@Table(name = "HOTEL")
public class Hotel extends AbstractEntity implements Serializable, Cloneable {

    @NotNull (message = "fill the name")
	private String name = "";

    @NotNull (message = "fill the address")
	private String address = "";

    @NotNull (message = "fill the rating")
	private Integer rating = 1;

    @NotNull (message = "fill the operatesFrom")
    @Column (name = "operates_from")
	private Long operatesFrom;

    @ManyToOne
	@JoinColumn(name = "category_id")
	private Category category;
	
	@NotNull (message = "fill the url")
	private String url;
	
	@Embedded
	private Payment payment;
	
	private String description;

	public boolean isPersisted() {
		return getId() != null;
	}

	@Override
	public String toString() {
		return name + " " + rating +"stars " + address;
	}

	@Override
	protected Hotel clone() throws CloneNotSupportedException {
		return (Hotel) super.clone();
	}

	public Hotel() {
		this("", "", 1, 0L, null, "", "");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public Long getOperatesFrom() {
		return operatesFrom;
	}

	public void setOperatesFrom(Long operatesFrom) {
		this.operatesFrom = operatesFrom;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}	

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public Payment getPayment() {
		return payment;
	}
	
	public void setPayment(Payment payment) {
		this.payment = payment;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Hotel(String name, String address, Integer rating, Long operatesFrom, Category category, String url, String description ) {
		super();
		this.name = name;
		this.address = address;
		this.rating = rating;
		this.operatesFrom = operatesFrom;
		this.category = category;
		this.url = url;
		this.description = description;
	}

	

}