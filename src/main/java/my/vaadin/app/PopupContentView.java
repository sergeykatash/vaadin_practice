package my.vaadin.app;

import java.lang.reflect.Field;
import java.time.Duration;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;

import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import my.vaadin.app.backend.Category;
import my.vaadin.app.backend.Hotel;
import my.vaadin.app.backend.HotelService;

public class PopupContentView extends Panel {
	
	private HotelService service = HotelService.getInstance();
	private NativeSelect<Field> fieldOfHotel = new NativeSelect<>();
	private TextField valueTxt = new TextField();
	private NativeSelect<Category> categoryValue = new NativeSelect<>();
	private DateField operatesFrom = new DateField(); 
	Set<Hotel> currentHotels;
	HotelView myView;
	
	public PopupContentView(HotelView myView) {
		this.myView = myView;
		
		setCaption("BULK UPDATE");
		
		categoryValue.setVisible(false);
		categoryValue.setDescription("choose a category");
		operatesFrom.setVisible(false);
		operatesFrom.setDescription("choose a date");
		
		fieldOfHotel.setDescription("choose a field");
		fieldOfHotel.setItems(Arrays.asList(Hotel.class.getDeclaredFields()));
		fieldOfHotel.setItemCaptionGenerator(Field::getName);
		categoryValue.setItems(service.getCategoriesL());
		
		fieldOfHotel.addSelectionListener(v -> {
			valueTxt.clear(); categoryValue.clear(); operatesFrom.clear();
			if (v.getSelectedItem().isPresent()){
			Class<?> typeOfField = v.getSelectedItem().get().getType();
			String typeOfFieldStr = typeOfField.getSimpleName();
			valueTxt.setVisible(typeOfFieldStr.equals("String") || typeOfFieldStr.equals("Integer"));
			categoryValue.setVisible(typeOfFieldStr.equals("Category"));
			operatesFrom.setVisible(typeOfFieldStr.equals("Long"));
			}
		});
		
		Button updateFieldBtn = new Button("Update");
	    Button closeBtn = new Button("Cancel");
	    
	    updateFieldBtn.addClickListener(event -> this.updateFieldOfHotel());
	    closeBtn.addClickListener(event -> this.closePopup());

	    valueTxt.setPlaceholder("Field value");
	    HorizontalLayout buttons = new HorizontalLayout(updateFieldBtn, closeBtn);
	    VerticalLayout panelForm = new VerticalLayout(fieldOfHotel, valueTxt, categoryValue, operatesFrom, buttons);
	    setContent(panelForm);
	    
	}
	
	//updating fields
	private void updateFieldOfHotel(){
		Optional<Field> choice = fieldOfHotel.getSelectedItem();
		if (choice.isPresent()){
			Field chosenField = choice.get();
			chosenField.setAccessible(true);
			String currentFieldName = chosenField.getName();
			
			//validating and saving field "rating"
			if (currentFieldName.equals("rating")){
				try{
				Integer newRating = Integer.parseInt(valueTxt.getValue());
				if ((newRating > 0) && (newRating < 6)){
					saveHotels(newRating, chosenField);
					closePopup();
				}else {
					Notification.show("invalid data");
				}
				}catch(NumberFormatException e){
					Notification.show("invalid format");
				}
			}else 
				
				//validating and saving field "category"
				if (currentFieldName.equals("category")){
					Optional<Category> categItem = categoryValue.getSelectedItem();
					if(categItem.isPresent()){
						saveHotels(categItem.get(), chosenField);
					}else {
						Notification.show("invalid data");
					}
				}else 
					
					//validating and saving field "operatesFrom"
					if (currentFieldName.equals("operatesFrom")){
						LocalDate data = operatesFrom.getValue();
						if (data != null){
							Long days = Duration.between(data.atTime(0, 0), LocalDate.now().atTime(0, 0)).toDays();
							saveHotels(days, chosenField);
						}else {
							Notification.show("invalid data");
						}
					} else 
						
						//validating and saving other string fields
						if (!valueTxt.getValue().isEmpty()){
						saveHotels(valueTxt.getValue(), chosenField);
						}else {
							Notification.show("not be empty");
						}		
		myView.updateListHotels();
		closePopup();	
		}else {
			Notification.show("choose a field");
		}
		
	}
	
	//saving current field of hotels in database
	private void saveHotels(Object value, Field chosenField){
		for(Hotel hotel : currentHotels){
			try {
				chosenField.set(hotel, value);
			} catch (IllegalArgumentException e) {
				Notification.show("invalid data");
			} catch (IllegalAccessException e) {
				Notification.show("invalid data");
			}
			service.save(hotel);
		}
	}
	
	private void closePopup(){
		setVisible(false);
		fieldOfHotel.setSelectedItem(null);
	}
	
	public void setHotels(Set<Hotel> currentHotels){
		fieldOfHotel.setSelectedItem(null);
		this.currentHotels = currentHotels;
		setVisible(true);
	}

}