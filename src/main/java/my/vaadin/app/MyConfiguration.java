package my.vaadin.app;

import org.springframework.context.annotation.Configuration;

import com.vaadin.spring.annotation.EnableVaadin;

@Configuration
@EnableVaadin    // this imports VaadinConfiguration
public class MyConfiguration {
   
    
}
