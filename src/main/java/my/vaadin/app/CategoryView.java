package my.vaadin.app;

import java.util.List;

import javax.annotation.PostConstruct;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.MultiSelectionModel;

import my.vaadin.app.backend.Category;
import my.vaadin.app.backend.HotelService;

@SpringView(name = CategoryView.VIEW_NAME)
public class CategoryView extends VerticalLayout implements View {
	public static final String VIEW_NAME = "category";
	private HotelService service = HotelService.getInstance();
	private Grid<Category> gridCategories = new Grid<>(Category.class);
	private CategoryForm formCategory = new CategoryForm(this);
	
	@PostConstruct
    void init() {

    	// select "multi" mode for grid of categories
    	MultiSelectionModel<Category> selectionModel = (MultiSelectionModel<Category>) gridCategories
    			.setSelectionMode(SelectionMode.MULTI);
    	
    	gridCategories.setColumns("name");
    	gridCategories.setItems(service.getCategoriesL());
   
    	Button addCategoryBtn = new Button("New");
    	addCategoryBtn.setId("newCategoryBtn");
        addCategoryBtn.addClickListener(e -> {
            gridCategories.asMultiSelect().clear();
            formCategory.setCategory(new Category());
        });    	
        
        Button editCategoryBtn = new Button("Edit");  
        editCategoryBtn.addClickListener(e -> {
            formCategory.setCategory((Category)gridCategories.asMultiSelect().getSelectedItems().iterator().next());
        });
        
        Button deleteCategoryBtn = new Button("Delete");  
        deleteCategoryBtn.addClickListener(e -> {
            service.deleteCategory(gridCategories.asMultiSelect().getSelectedItems());
            updateListCategories();
            //updateListHotels();
        });
        
        //access to buttons
        selectionModel.addSelectionListener(v -> {
        	formCategory.closeCategoryForm();
        	int count = selectionModel.getSelectedItems().size();
        	editCategoryBtn.setEnabled(count == 1);
        	deleteCategoryBtn.setEnabled(count != 0);
        });
        
        //arrangement of components
        HorizontalLayout mainCategories = new HorizontalLayout(gridCategories, formCategory);
        HorizontalLayout buttonsCategories = new HorizontalLayout(addCategoryBtn, editCategoryBtn, deleteCategoryBtn);
    	addComponents(buttonsCategories, mainCategories);
    	editCategoryBtn.setEnabled(false); deleteCategoryBtn.setEnabled(false);
    	updateListCategories();
    	formCategory.setVisible(false);	
	}
	
	//update grid of categories
	public void updateListCategories() {
		List<Category> listOfCategories = service.getCategoriesL();
		gridCategories.setItems(listOfCategories);
		
	}
	
	

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		
	}

}
