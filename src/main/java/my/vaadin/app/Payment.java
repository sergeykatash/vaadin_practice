package my.vaadin.app;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Payment {

	@Column(name = "PAYMENT")
	private Integer payment;
	
	 public Integer getPayment() {
		return payment;
	}
	 
	 public void setPayment(Integer payment) {
		this.payment = payment;
	}
	 
	 public Payment() {
		super();
	}
	 
	 public Payment(Integer payment) {
			super();
			this.payment = payment;
		} 
}