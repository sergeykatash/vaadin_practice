package my.vaadin.app;

import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.Label;
import com.vaadin.ui.RadioButtonGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class PaymentField extends CustomField<Payment>  {

	private RadioButtonGroup<String> choice = new RadioButtonGroup<>();
	private TextField text = new TextField();
	private Label label = new Label("Payment will be made directly in the hotel");
	private Payment value = new Payment();
	
	
	@Override
	public Payment getValue() {
		// TODO Auto-generated method stub
		return value;
	}

	@Override
	protected Component initContent() {
		VerticalLayout layout = new VerticalLayout();
		choice.setItems("Credit Card", "Cash");
		choice.setSelectedItem("Cash");
		choice.addStyleName(ValoTheme.OPTIONGROUP_HORIZONTAL);
		text.setVisible(false);
		text.setDescription("advance payment percent");
		layout.addComponents(choice, text, label);
		
		choice.addSelectionListener(listener -> {
			String ch = listener.getValue();
		if (ch.equals("Cash")){
			value.setPayment(null);
			setValue(value);
			label.setVisible(true);
			text.setVisible(false);
		}else {
			label.setVisible(false);
			text.setVisible(true);
		}
		});
		
		text.addValueChangeListener(listener -> {
			if (!listener.getValue().equals("")){
				Payment vvv = new Payment(value.getPayment());
				value.setPayment(Integer.parseInt(listener.getValue()));
				setValue(value);
				fireEvent(new ValueChangeEvent<Payment>(this, vvv, false));
			}
		});
		
		updateValues();
		return layout;
	}

	private void updateValues() {
		if (value == null || value.getPayment() == null) {
			text.clear();
			choice.setSelectedItem("Cash");
		}else {
			choice.setSelectedItem("Credit Card");
			text.setValue(value.getPayment().toString());
		}
	}

	@Override
	protected void doSetValue(Payment val) {
		value = new Payment();
		value.setPayment(val == null ? null : val.getPayment());
		updateValues();

	}

}
