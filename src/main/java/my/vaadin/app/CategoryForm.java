package my.vaadin.app;

import com.vaadin.data.Binder;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import my.vaadin.app.backend.Category;
import my.vaadin.app.backend.HotelService;
/**
*/
public class CategoryForm extends FormLayout{
	
	private TextField categoryField = new TextField("Category");
	private Button newCategory = new Button("Save");
	private Button closeCategoryForm = new Button("Cancel");
	private Binder<Category> binder = new Binder<>(Category.class);
	
	private HotelService service = HotelService.getInstance();
	private Category category;
    private CategoryView view;
	
	public CategoryForm(CategoryView view) {
		this.view = view;
		
		categoryField.setDescription("category of a hotel");
		categoryField.setId("categoryField");
		HorizontalLayout buttons = new HorizontalLayout(newCategory, closeCategoryForm);
		addComponents(categoryField, buttons); 
		
		newCategory.setId("saveCategory");
		newCategory.setStyleName(ValoTheme.BUTTON_PRIMARY);
		newCategory.setClickShortcut(KeyCode.ENTER);
		
		//bind field with class category
		binder.forField(categoryField).asRequired("not be empty")
    	.bind(Category::getName, Category::setName);

		
		newCategory.addClickListener(e -> this.saveCategory());
		closeCategoryForm.addClickListener(e -> this.closeCategoryForm());
		
	}
	
	public void setCategory(Category category) {
		this.category = category;
		binder.setBean(category);
		setVisible(true);
	}
	
	public void closeCategoryForm() {
		binder.removeBean();
	    setVisible(false);
	}
	
	//save or edit category from form
	private void saveCategory() {
	    service.saveCategory(category);
	    view.updateListCategories();
	    setVisible(false);
	}
	
}
