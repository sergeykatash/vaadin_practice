package my.vaadin.app;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class AdditionDataUITest extends AbstractUITest  {

	@Test
	public void addDataTest() throws InterruptedException{
		driver.get(BASE_URL);
		Thread.sleep(3000);
		
		WebElement categoryMenu = driver.findElement(By.xpath("//span[@class='v-menubar-menuitem'][./span/@class='v-menubar-menuitem-caption'][contains(./span/text(), 'Categories')]"));
		categoryMenu.click();
		Thread.sleep(1000); 
		
		Assert.assertEquals("http://localhost:8080/#!category", driver.getCurrentUrl());
		
		String[] dataCategory = {"Category_1", "Category_2", "Category_3"};
		
		for (String category : dataCategory){
			WebElement categoryNew = driver.findElement(By.id("newCategoryBtn"));
			categoryNew.click();
			Thread.sleep(1000);
			
			WebElement categoryField = driver.findElement(By.id("categoryField"));
			categoryField.sendKeys(category);
			Thread.sleep(1000);
			
			WebElement categorySave = driver.findElement(By.id("saveCategory"));	
			categorySave.click();
			Thread.sleep(1000);
		}
		
		WebElement hotelMenu = driver.findElement(By.xpath("//span[@class='v-menubar-menuitem'][./span/@class='v-menubar-menuitem-caption'][contains(./span/text(), 'Hotels')]"));
		hotelMenu.click();
		Thread.sleep(1000);
		
		Assert.assertEquals("http://localhost:8080/#!", driver.getCurrentUrl());
		
		String[] hotelCategory = {"Hotel_1;3;https://www.booking.com;Address_1",
				                  "Hotel_2;2;https://www.booking.com;Address_2"};
		
		for (String hotel : hotelCategory){
			WebElement hotelNew = driver.findElement(By.id("addHotelBtn"));
			hotelNew.click();
			Thread.sleep(1000);
			
			Random r = new Random();
			String[] split = hotel.split(";");
			
			WebElement hotelName = driver.findElement(By.id("name"));
			hotelName.sendKeys(split[0]);
			WebElement hotelAddress = driver.findElement(By.id("address"));
			hotelAddress.sendKeys(split[3]);
			WebElement hotelRating = driver.findElement(By.id("rating"));
			hotelRating.clear();
			hotelRating.sendKeys(split[1]);
			WebElement hotelCateg = driver.findElement(By.xpath("//*[@id='category']/select"));
			Select select = new Select(hotelCateg);
			select.selectByIndex(r.nextInt(select.getOptions().size()-1) + 1);
			WebElement hotelOperatesFrom = driver.findElement(By.xpath("//*[@id='operatesFrom']/input"));
			hotelOperatesFrom.clear();
			long daysOld = r.nextInt(365 * 30);
			hotelOperatesFrom.sendKeys((LocalDate.now().plusDays(daysOld).format(DateTimeFormatter.ofPattern("dd.MM.uu"))));
			WebElement hotelUrl = driver.findElement(By.id("url"));
			hotelUrl.sendKeys(split[2]);
			Thread.sleep(2000);
			
			WebElement hotelSave = driver.findElement(By.id("saveHotel"));	
			hotelSave.click();
			Thread.sleep(1000);
	    }
		
	}
}
